Europass
--------

The Europass module collects field values from one or many entity bundles and
uses the API provided as web-services exposed by the Europass Interoperability
[http://interop.europass.cedefop.europa.eu] in order to allow users to download
the formatted Europass Curriculum Vitae. The module allows users to download the
Curriculum Vitae as .pdf, .doc or .odt but exposes an API that allows developers
to provide additional output formats.

Configure
---------

After installing the module:

1. Go to admin/config/content/europass and verify or update the web-services
   URLs. Select the bundle or bundles that you want to act as Curriculum Vitae.
2. Permissions. The module exposes a permission for each output format. You can
   decide the output formats allowed per role. Default permissions provided by
   the module: 'access pdf europass', 'access doc europass' and
   'access odt europass'.
3. Edit the fields, from the previously selected bundles, and map each field to
   a Europass Json Schema field using the 'Map an Europass schema field' select
   element. It's possible that the select element is missed. That means that the
   Drupal field type is not defined via hook_europass_mapping(). You'll need to
   understand the structure of an Europass Xml/Json document. Read more about
   that on Europass Interoperability [http://interop.europass.cedefop.europa.eu]
   and understand the structure of Europass Json Schema.

Author & Sponsor
----------------

Author: Claudiu Cristea
- Drupal: https://www.drupal.org/u/claudiu.cristea
- Twitter: @claudiu_cristea

Sponsor: Webikon.com | http://webikon.com

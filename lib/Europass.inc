<?php

/**
 * @file
 * Contains Europass.
 */


/**
 * Class Europass.
 */
class Europass {

  /**
   * The schema internal representation.
   *
   * @var stdClass
   */
  protected $schema;

  /**
   * The Europass internal representation.
   *
   * @var stdClass
   */
  protected $europass;

  /**
   * Pointer to the current Europass node being processed.
   *
   * @var mixed
   */
  protected $pointer;

  /**
   * Flag indicating if the build of Europass has been initialized.
   *
   * @var bool
   */
  protected $initialized = FALSE;

  /**
   * Returns the Europass as internal representation.
   *
   * @return stdClass
   *   The Europass internal representation.
   */
  public function getEuropass() {
    return $this->europass;
  }

  /**
   * Builds an Europass internal representation based on values stored inside an
   * entity.
   *
   * @param string $type
   *   The entity type.
   * @param string $bundle
   *   The entity bundle.
   * @param object $entity
   *   The entity object.
   */
  public function build($type, $bundle, $entity) {
    if (!$this->initialized) {
      $this->init();
    }

    ctools_include('plugins');

    foreach (field_info_instances($type, $bundle) as $field_name => $info) {
      if (!empty($info['settings']['europass_mapping'])) {
        $field = empty($entity->$field_name) ? NULL : $entity->$field_name;

        if (!empty($field)) {
          $path = explode('.', $info['settings']['europass_mapping']);
          $context = array(
            'entity_type' => $type,
            'bundle' => $bundle,
            'entity' => $entity,
          );
          $this->add($field_name, $field, $path, $context);
        }
      }
    }
  }

  /**
   * Initializes the schema.
   */
  public function init() {
    $this->schema = EuropassUtility::getSchema();
    $this->europass = new stdClass();
    $this->initialized = TRUE;
  }

  public function render() {
    return json_encode($this->europass);
  }

  /**
   * Adds a new value to Europass Json.
   *
   * @param string $field_name
   *   The Drupal filed name.
   * @param array $field
   *   The Drupal field contents to be added to Europass.
   * @param array $path
   *   The schema path.
   * @param array $context
   *   Context values (entity type, bundle, ...).
   * @param stdClass|null $schema
   *   Internally used in recursion process.
   */
  protected function add($field_name, array $field, array $path, array $context, $schema = NULL) {
    // Initialize a copy of schema. This will be altered during recursions.
    if (is_null($schema)) {
      $schema = clone $this->schema;
      $this->pointer =& $this->europass;
    }

    $property = array_shift($path);
    if (empty($property)) {
      return;
    }

    if (!empty($schema->properties->$property)) {
      $schema = $schema->properties->$property;
      if (!empty($schema->type)) {
        switch ($schema->type) {
          case 'object':
            if (!isset($this->pointer->$property)) {
              $this->pointer->$property = new stdClass();
            }
            $this->pointer =& $this->pointer->$property;
            break;
          case 'array':
            if (!isset($this->pointer[$property])) {
              $this->pointer[$property] = array();
            }
            $this->pointer =& $this->pointer[$property];
            break;
          case 'string':
            $this->pointer->$property = $this->process($field_name, $field, $schema->type, $context);
            break;
        }

        if ($path) {
          $this->add($field_name, $field, $path, $context, $schema);
        }
      }
    }
  }

  /**
   * Processes a value using callbacks defined in mappings.
   *
   * @param string $field_name
   *   The Drupal filed name.
   * @param array $field
   *   The Drupal field contents to be added to Europass.
   * @param string $schema_type
   *   The Json Schema type.
   * @param array $context
   *   Context values (entity type, bundle, ...).
   *
   * @return mixed
   *   The value to be stored.
   *
   * @throws \Exception
   */
  protected function process($field_name, array $field, $schema_type, array $context) {
    $info = field_info_field($field_name);
    $type = $info['type'];

    $map = EuropassUtility::getTypes($type);
    $map = $map[$schema_type];

    if (empty($map['processors'])) {
      $map['processors'] = array('generic');
    }
    $map['processors'] = array_unique($map['processors']);

    $value = NULL;
    foreach ($map['processors'] as $plugin_id) {
      $plugin = ctools_get_plugins('europass', 'europass_processor', $plugin_id);
      $class = ctools_plugin_get_class($plugin, 'handler');
      if (!in_array('EuropassProcessorInterface', class_implements($class))) {
        throw new \Exception(sprintf("Class '%s' should implement 'EuropassProcessorInterface' interface.", $class));
      }

      $callback = array($class, 'process');
      $value = call_user_func($callback, $field_name, $field, $value, $context);
    }

    return $value;
  }

  /**
   * Provides a generic processor.
   *
   * @param string $field_name
   *   The Drupal filed name.
   * @param array $field
   *   The Drupal field.
   * @param mixed $value
   *   The value from te previous callback result. On the first callback this
   *   value will be NULL.
   * @param array $context
   *   Context values (entity type, bundle, ...).
   *
   * @return mixed
   *   The processed value.
   */
  protected function genericProcess($field_name, array $field, $value, array $context) {
    $langcode = field_language($context['entity_type'], $context['entity'], $field_name);
    return empty($field[$langcode][0]['value']) ? NULL : $field[$langcode][0]['value'];
  }

}

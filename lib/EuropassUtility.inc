<?php

/**
 * @file
 * Contains EuropassUtility.
 */

/**
 * Class EuropassUtility.
 */
class EuropassUtility {

  /**
   * Minimum Europass schema to be used.
   *
   * @var string
   */
  const MIN_SCHEMA = 'http://europass.cedefop.europa.eu/json/schema/v3.2.0/Europass_JSONSchema.json';

  /**
   * Default services.
   *
   * @var array
   */
  static public $services = array(
    'pdf' => 'https://europass.cedefop.europa.eu/rest/v1/document/to/pdf',
    'doc' => 'https://europass.cedefop.europa.eu/rest/v1/document/to/word',
    'odt' => 'https://europass.cedefop.europa.eu/rest/v1/document/to/opendoc',
  );

  /**
   * List of accepted languages.
   *
   * @var array
   */
  static public $acceptedLanguages = array(
    'bg', 'es', 'cs', 'da', 'de', 'et', 'el', 'en', 'fr', 'hr', 'is', 'it',
    'lv', 'lt', 'hu', 'mt', 'nl', 'no', 'pl', 'pt', 'ro', 'sk', 'sl', 'fi',
    'sv', 'tr'
  );

  /**
   *
   */
  /**
   * Gets the schema and stores it in Drupal cache for later use.
   *
   * @param string|null $url
   *   (optional) Schema URL to read from. If missed, the stored URL will be used.
   *
   * @return stdClass|string
   *   The schema as associative array or an error message on failure.
   */
  static public function getSchema($url = NULL) {
    $url = $url ?: variable_get('europass_schema_url', static::MIN_SCHEMA);

    if (empty($url)) {
      $path = url('admin/config/content/europass', array('absolute' => TRUE));
      $schema = t('Configure the JSON schema URL at <a href="@path">@path</a>.', array('@path' => $path));
    }
    else {
      if ($cache = cache_get('europass_schema')) {
        $schema = $cache->data;
      }
      else {
        $response = drupal_http_request($url);
        if ($response->code != 200 || empty($response->data)) {
          $schema = t('No valid data was returned from @url. Returned code: @code.', array('@url' => $url, '@code' => $response->code));
        }
        else {
          // We are not using the drupal_json_decode() wrapper because we need the
          // preserve the result as stdClass.
          // @see json_decode()
          // @see drupal_json_decode()
          $schema = json_decode($response->data);

          // Resolve references inside schema.
          $resolver = new ReferenceResolver($schema);
          $resolver->resolve();

          // Cache schema.
          cache_set('europass_schema', $schema);
        }
      }
    }

    return $schema;
  }

  /**
   * Builds a list of Europass fields that can be mapped to a specific Drupal
   * field type.
   *
   * @param string $field_type
   *   The Drupal field type.
   * @param stdClass $schema
   *   The schema to be searched for candidate fields.
   *
   * @return array
   *   An associative array suitable for a FAPI select element '#options' value.
   */
  static public function getCandidates($field_type, stdClass $schema, $path = array()) {
    static $options = array();

    if (!$types = static::getTypes($field_type)) {
      return array();
    }
    $types = array_keys($types);

    foreach ($schema as $key => $item) {
      $path[] = $key;

      if (empty($item->type)) {
        continue;
      }

      $type = $item->type;

      if (in_array($type, $types)) {
        $trail = implode('.', $path);
        $options[$trail] = $trail;
      }
      elseif (in_array($type, array('array', 'object')) && isset($item->properties) && is_object($item->properties)) {
        static::getCandidates($field_type, $item->properties, $path);
      }

      array_pop($path);
    }

    return $options;
  }

  /**
   * Gets the JSON types that are qualifying for a specific Drupal field type.
   *
   * @param string $field_type
   *   (optional) The Drupal field type. Eg. 'text', 'text_long',
   *   'number_integer', etc. If omitted the whole types mappings definitions
   *   are returned.
   *
   * @return array
   *   A list of JSON type mappings.
   */
  static public function getTypes($field_type = NULL) {
    static $map;

    if (!isset($map)) {
      if ($cache = cache_get('europass_mapping')) {
        $map = $cache->data;
      }
      else {
        // Collect types from modules.
        $map = module_invoke_all('europass_mapping');

        // Allow modules alter the results.
        drupal_alter('europass_mapping', $map);

        // Normalize Json type items that have no settings.
        foreach ($map as $field => $types) {
          foreach ($types as $type => $info) {
            if (is_string($info)) {
              $map[$field][$info] = array();
              unset($map[$field][$type]);
            }
          }
        }

        // Cache results for later use.
        cache_set('europass_mapping', $map);
      }
    }

    if (empty($field_type)) {
      return $map;
    }

    return isset($map[$field_type]) ? $map[$field_type] : NULL;
  }

  /**
   * Checks if a bundle qualifies for Europass.
   *
   * The bundle must met both conditions:
   * - Was marked as Europass bundle.
   * - Has at least one filed mapped to an Europas Schema key.
   *
   * @param string $type
   *   The entity type.
   * @param string $bundle
   *   The bundle ID.
   *
   * @return bool
   *   If the given bundle qualifies as Europass.
   */
  static public function bundleQualifies($type, $bundle) {
    return static::isMappable($type, $bundle) && static::hasMappings($type, $bundle);
  }

  /**
   * Checks whenever a bundle is subject of Europass.
   *
   * @param string $type
   *   The entity type.
   * @param string $bundle
   *   The bundle ID.
   *
   * @return bool
   *   If the given bundle is a Curriculum Vitae bundle.
   */
  static public function isMappable($type, $bundle) {
    foreach (variable_get('europass_bundles', array()) as $item) {
      list($stored_type, $stored_bundle) = explode(':', $item);
      if ($stored_type == $type && $stored_bundle == $bundle) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Checks if a bundle has mapped fields.
   *
   * @param string $type
   *   The entity type.
   * @param string $bundle
   *   The bundle ID.
   *
   * @return bool
   *   If the given bundle has mapped fields.
   */
  static protected function hasMappings($type, $bundle) {
    foreach (field_info_instances($type, $bundle) as $id => $info) {
      if (!empty($info['settings']['europass_mapping'])) {
        return TRUE;
      }
    }
    return FALSE;
  }

}

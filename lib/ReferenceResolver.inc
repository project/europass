<?php

/**
 * @file
 * Contains ReferenceResolver.
 */


/**
 * Class ReferenceResolver.
 *
 * Some parts were inspired from https://github.com/justinrainbow/json-schema.
 */
class ReferenceResolver {

  /**
   * Maximum references depth.
   *
   * @var integer
   */
  const MAX_DEPTH = 7;

  /**
   * The whole schema object.
   *
   * @var stdClass|null
   */
  protected $schema = NULL;

  /**
   * Prevent too many recursive expansions. Happens when you want to validate a
   * schema against the schema definition.
   *
   * @var integer
   */
  protected $depth = 0;

  /**
   * Constructs a ReferenceResolver object.
   *
   * @param stdClass $schema
   *   The schema object to be resolved.
   */
  public function __construct(stdClass $schema) {
    $this->schema = $schema;
  }

  /**
   * Resolves all $ref references for a given schema.  Recurses through the
   * object to resolve references of any child schemas.
   *
   * The 'format' property is omitted because it isn't required for validation.
   *
   * @param mixed $schema
   *   (optional) Schema as a standard class. If not passed, then we'll use the
   *   instance schema as a start.
   */
  public function resolve($schema = NULL) {
    // If $schema was not passed then use the schema passed when the resolver
    // has been constructed. This allows the following usage of the class:
    // @code
    // $resolver = new ReferenceResolver($schema);
    // $resolver->resolve();
    // @endcode
    if (is_null($schema)) {
      $schema = $this->schema;
    }

    if ($this->depth > static::MAX_DEPTH) {
      return;
    }
    ++$this->depth;

    if (!is_object($schema)) {
      --$this->depth;
      return;
    }

    // Resolve $ref first
    $this->resolveReference($schema);

    // These properties are just schemas. Eg. items can be a schema or an array
    // of schemas.
    $items = array('additionalItems', 'additionalProperties', 'extends', 'items');
    foreach ($items as $item) {
      $this->resolveProperty($schema, $item);
    }

    // These are all potentially arrays that contain schema objects. Eg. type
    // can be a value or an array of values/schemas. Eg. items can be a schema
    // or an array of schemas.
    $items = array('disallow', 'extends', 'items', 'type', 'allOf', 'anyOf', 'oneOf');
    foreach ($items as $item) {
      $this->resolveArrayOfSchemas($schema, $item);
    }

    // These are all objects containing properties whose values are schemas.
    $items = array('dependencies', 'patternProperties', 'properties');
    foreach ($items as $item) {
      $this->resolveObjectOfSchemas($schema, $item);
    }

    --$this->depth;
  }

  /**
   * Given an object and a property name, that property should be an array whose
   * values can be schemas.
   *
   * @param stdClass $schema
   *   The schema object.
   * @param string $property
   *   The property to work on.
   */
  protected function resolveArrayOfSchemas($schema, $property) {
    if (!isset($schema->$property) || !is_array($schema->$property)) {
      return;
    }

    foreach ($schema->$property as $possiblySchema) {
      $this->resolve($possiblySchema);
    }
  }

  /**
   * Given an object and a property name, that property should be an object
   * whose properties are schema objects.
   *
   * @param stdClass $schema
   *   The schema object.
   * @param string $property
   *   The property to work on.
   */
  protected function resolveObjectOfSchemas($schema, $property) {
    if (!isset($schema->$property) || !is_object($schema->$property)) {
      return;
    }

    foreach (get_object_vars($schema->$property) as $possiblySchema) {
      $this->resolve($possiblySchema);
    }
  }

  /**
   * Given an object and a property name, that property should be a schema
   * object.
   *
   * @param stdClass $schema
   *    The schema object.
   * @param string $property
   *    Property to work on.
   */
  protected function resolveProperty($schema, $property) {
    if (!isset($schema->$property)) {
      return;
    }

    $this->resolve($schema->$property);
  }

  /**
   * Look for the $ref property in the object. If found, remove the reference
   * and augment this object with the contents of another schema.
   *
   * @param stdClass $schema
   *   The schema.
   */
  protected function resolveReference($schema) {
    $reference = '$ref';

    if (empty($schema->$reference)) {
      return;
    }

    $referenced_schema = $this->fetchReference($schema->$reference);
    unset($schema->$reference);

    // Augment the current $schema object with properties fetched
    foreach (get_object_vars($referenced_schema) as $property => $value) {
      $schema->$property = $value;
    }
  }

  /**
   * Retrieves a schema given a reference.
   *
   * @param string $reference
   *   Reference from schema.
   *
   * @return stdClass
   *   The referenced schema object.
   */
  protected function fetchReference($reference) {
    $schema = $this->getReferencedSchema($reference);
    $this->resolve($schema);

    return $schema;
  }

  /**
   * Resolve a schema based on pointer.
   *
   * URIs can have a fragment at the end in the format of #/path/to/object and
   * we are to look up the 'path' property of the first object then the 'to' and
   * 'object' properties.
   *
   * @param string $uri
   *  The schema reference URI.
   *
   * @return stdClass
   *  Schema after walking down the fragment pieces.
   *
   * @throws \Exception
   */
  protected function getReferencedSchema($uri) {
    $schema = $this->schema;
    $fragment = ltrim($uri, '#');
    if (empty($fragment)) {
      return $this->schema;
    }

    $path = explode('/', $fragment);
    while ($path) {
      $element = array_shift($path);
      if (empty($element)) {
        continue;
      }

      $element = str_replace('~1', '/', $element);
      $element = str_replace('~0', '~', $element);

      if (!empty($schema->$element)) {
        $schema = $schema->$element;
      }
      else {
        throw new \Exception(sprintf("Fragment '%s' not found in '%s'.", $element, $uri));
      }

      if (!is_object($schema)) {
        throw new \Exception(sprintf("Fragment part '%s' is no object in '%s'.", $element, $uri));
      }
    }

    return $schema;
  }

}

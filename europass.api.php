<?php

/**
 * @file
 * Contains documents API functions for Europass module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Provides mappings between Drupal field types and Europass field types.
 *
 * Modules can define their own mappings by implementing this hook.
 *
 * @return array
 *   The mapping represented as associative array, having the Drupal field type
 *   as keys and an array of possible Json Schema types as values. Each element
 *   of such array can be either an array, with Json type name ('string',
 *   'number', etc.) as key, either a simple string, if that needs no additional
 *   info. Providing a string is just a shorthand. If the element is an array,
 *   it will provide the next keys:
 *   - 'processors': (optional) An array of Chaos Tools (ctools) plugins of type
 *     'europass_processor' to be used for field value processing. If omitted,
 *     Drupal will add the default  generic processor 'generic'. Modules can
 *     define additional plugins. The handler class should conform the interface
 *     described  by EuropassProcessorInterface interface.
 *
 * @see EuropassProcessorInterface
 * @see europass_ctools_plugin_type()
 */
function mymodule_europass_mapping() {
  $mapping = array(
    'text' => array(
      'string' => array(
        'processors' => array('text_plugin'),
      ),
      'integer',
    ),
    'number_integer' => array(
      'integer' => array(),
    ),
  );

  return $mapping;
}

/**
 * Allows modules to alter the built mapping.
 *
 * @param array $mapping
 *   The defined mapping passed by reference. You'll need to alter this array.
 */
function mymodule_europass_mapping_alter(&$mapping) {
  $mapping['text'][] = 'number';
  $mapping['text']['integer'] = array('processors' => array('otherplugin'));
}

/**
 * Provides additional formats to be used as Europass output.
 *
 * @return array
 *   An associative array keyed by the format Id ('xml', 'pdf', 'doc', ...) and
 *   having an associative array as values with the next keys:
 *   - 'name': A name/label fot this output format.
 *   - 'description': A more detailed description.
 *   - 'extension': The file extension, without dot, used by this output type.
 *     This extension is used name the downloaded file.
 *   - 'mime': The mime type. Used to set the HTTP 'Content-Type' header when
 *     downloading the file.
 */
function mymodule_europass_format() {
  return array(
    'xml' => array(
      'name' => t('XML'),
      'description' => t('XML format.'),
      'extension' => 'xml',
      'mime' => 'application/xml',
    ),
  );
}

/**
 * Allows modules to alter the format list.
 *
 * @param array $formats
 *   The defined list of formats passed by reference. You'll need to alter this
 *   array.
 */
function mymodule_europass_format_alter(&$formats) {
  // Use 'Microsoft' instead of 'MS'.
  $formats['doc']['name'] = t('Microsoft Word');
}

/**
 * @} End of "addtogroup hooks".
 */

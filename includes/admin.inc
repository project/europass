<?php

/**
 * @file
 * Contains administrative tasks for Europass module.
 */

/**
 * Form API callback for 'admin/config/content/europass' route.
 *
 * @param array $form
 *   The Form API form array.
 * @param array $form_state
 *   The Form API form state array.
 *
 * @return array
 *   A renderable representing the form.
 */
function europass_settings($form, &$form_state) {
  $form['url'] = array(
    '#type' => 'value',
    '#value' => variable_get('europass_schema_url', EuropassUtility::MIN_SCHEMA),
  );
  $form['bundles'] = array(
    '#type' => 'value',
    '#value' => variable_get('europass_bundles', array()),
  );

  $form['europass_schema_url'] = array(
    '#type' => 'textfield',
    '#title' => t('JSON schema URL'),
    '#description' => t('Enter the JSON schema URL to be used from here: <a href="http://interop.europass.cedefop.europa.eu/data-model/json-resources">http://interop.europass.cedefop.europa.eu/data-model/json...</a>.'),
    '#default_value' => variable_get('europass_schema_url', EuropassUtility::MIN_SCHEMA),
    '#required' => TRUE,
  );

  $options = array();
  foreach (entity_get_info() as $type => $info) {
    $options[$info['label']] = array();
    foreach ($info['bundles'] as $bundle_id => $bundle) {
      $options[$info['label']][$type . ':' . $bundle_id] = $bundle['label'];
    }
  }

  $form['europass_bundles'] = array(
    '#type' => 'select',
    '#title' => t('Entity bundles acting as Europass'),
    '#description' => t('Fields of selected bundles can be mapped to Europass fields. Each of selected bundles can act as a Curriculum Vitae.'),
    '#options' => $options,
    '#multiple' => TRUE,
    '#default_value' => variable_get('europass_bundles', array()),
    '#size' => 12,
  );

  // Webservices URLs.
  $form['europass_services'] = array(
    '#type' => 'fieldset',
    '#title' => t('Europass generation webservices'),
    '#description' => t('Enter the webservices URLs.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );

  $services = variable_get('europass_services', EuropassUtility::$services);
  foreach (europass_get_format() as $id => $format) {
    $form['europass_services'][$id] = array(
      '#type' => 'textfield',
      '#title' => $format['name'],
      '#description' => $format['description'],
      '#default_value' => $services[$id],
    );
  }

  $form = system_settings_form($form);

  // Adding an additional validation & submit callbacks.
  $form['#validate'][] = 'europass_settings_validate';
  $form['#submit'][] = 'europass_settings_submit';

  return $form;
}

/**
 * Additional Form API validation callback for 'admin/config/content/europass'
 * route.
 *
 * We are storing the JSON schema on Drupal side for better performance.
 *
 * @param array $form
 *   The Form API form array.
 * @param array $form_state
 *   The Form API form state array.
 */
function europass_settings_validate($form, &$form_state) {
  $schema = EuropassUtility::getSchema($form_state['values']['europass_schema_url']);

  if (is_string($schema)) {
    form_set_error('europass_schema_url', $schema);
  }
}

/**
 * Additional Form API submit callback for 'admin/config/content/europass'
 * route.
 *
 * Just clearing the extra fields cache.
 *
 * @param array $form
 *   The Form API form array.
 * @param array $form_state
 *   The Form API form state array.
 */
function europass_settings_submit($form, &$form_state) {
  $values =& $form_state['values'];
  if ($values['europass_schema_url'] != $values['url']) {
    cache_clear_all('europass_schema');
  }
  if ($values['europass_bundles'] != $values['bundles']) {
    cache_clear_all('field_info:bundle_extra:', 'cache_field', TRUE);
  }
}

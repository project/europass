<?php

/**
 * @file
 * Contains download callbacks for Europass.
 */

/**
 * Provides a route callback for downloading Europass.
 *
 * @param string $type
 *  The entity type.
 * @param string $bundle
 *   The bundle.
 * @param object $entity
 *   The entity object.
 * @param array $format
 *   The Europass format information.
 *
 * @return mixed
 */
function europass_download($type, $bundle, $entity, array $format) {
  global $language;

  $europass = new Europass();
  $europass->build($type, $bundle, $entity);
  $json = $europass->render();

  $langcode = isset(EuropassUtility::$acceptedLanguages[$language->language]) ? EuropassUtility::$acceptedLanguages[$language->language] : 'en';
  $urls = variable_get('europass_services', EuropassUtility::$services);
  $options = array(
    'method' => 'POST',
    'headers' => array(
      'Content-Type' => 'application/json',
      'Accept-Language' => $langcode,
    ),
    'data' => $json,
  );
  $url = $urls[$format['id']];

  $response = drupal_http_request($url, $options);

  if ($response->code != 200 || empty($response->data)) {
    drupal_add_http_header('Status', '503 Service unavailable');
    $title = t('Europass service error');
    drupal_set_title($title);

    $items = array(
      t('Request') . ':' . '<div><pre>' . $response->request . '</pre></div>',
      t('Code: @code', array('@code' => $response->code)),
    );
    if (!empty($response->error)) {
      $items[] = t('Message: @message', array('@message' => $response->error));
    }
    watchdog('europass', theme('item_list', array('items' => $items, 'title' => $title)), array(), WATCHDOG_CRITICAL);

    return array(
      '#type' => 'markup',
      '#markup' => t('A call to Europass public services returns an error. Please contact de site administrator.'),
    );
  }

  $filename = 'europass.' . $format['extension'];
  drupal_add_http_header('Content-Type', $format['mime']);
  drupal_add_http_header('Content-Disposition', 'attachment; filename="' . $filename . '"');

  print $response->data;
  drupal_exit();
}

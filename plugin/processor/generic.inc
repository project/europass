<?php

/**
 * @file
 * Contains 'generic' plugin.
 */

$plugin = array(
  'label' => t('Generic'),
  'description' => t('Generic processor.'),
  'handler' => array(
    'class' => 'EuropassProcessorGeneric',
  ),
);

/**
 * Class EuropassProcessorGeneric.
 */
class EuropassProcessorGeneric implements EuropassProcessorInterface {

  /**
   * {@inheritdoc}
   */
  static public function process($field_name, array $field, $value, array $context) {
    $langcode = field_language($context['entity_type'], $context['entity'], $field_name);
    return empty($field[$langcode][0]['value']) ? NULL : $field[$langcode][0]['value'];
  }

}
